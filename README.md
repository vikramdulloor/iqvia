# IQVIA

A Seed Project Which can be extended to a Mid Scale to large Scale application.

## Configuration Steps for C9.io

    1. Run `npm install`
    2. Open terminal, navigate to workspacke and run `./mongod
    3. Run `npm start` to run application
    4. Run `npm run test` to test the application
**Your application will open [here](https://iqvia-dulloor.c9users.io/)**

## Configuration Steps for local desktop

    1. Download the application from [here](https://bitbucket.org/vikramdulloor/iqvia)
    2. Before running application, make sure you have Node, NPM and MongoDb installed.
    3. Navigate to the application Path and run `npm install`
    4. Start MongoDb service
    5. Run `npm start` to run application
    6. Run `npm run test` to test the application