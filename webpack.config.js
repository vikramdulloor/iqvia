const path = require('path');

module.exports = {
    entry: {
        main: './src/main.ts'
    },
    resolve: {
        extensions: ['.js', '.ts', '.html']
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                use: [{ loader: 'html-loader' }]
            },
            {
                test: /\.css$/,
                use: [{ loader: 'raw-loader' }]
            }
        ],
        exprContextCritical: false
    },
}
