const passport    = require('passport');
const passportJwt = require('passport-jwt');
const User        = require('../models/user');

const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;
const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'c1FJhlEVCl8hud031aHc'
};
module.exports = new JwtStrategy(opts, function(jwt_payload, done) {
    User.getUserById(jwt_payload._id, function(err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
});