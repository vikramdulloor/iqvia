export class Position{
    constructor(
        public fullName: string,
        public designation: string,
        public searchControl: string,
        public age: number,
        public startDate: Date,
        public salary: number,
        public latitude: number,
        public longitude: number,
        public _id?: string
    ){}
}