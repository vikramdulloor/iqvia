import { PositionService } from './positions.service';
import { Position } from './positions.model';
import { Component, OnInit, Renderer, ViewChild, TemplateRef, ElementRef } from '@angular/core';
declare var jquery:any;
declare var $ :any;

@Component({
    selector: 'iqvia-position',
    templateUrl: './positions.component.html',
    providers: [PositionService],
    styles:[`
        .pointer{
            cursor: pointer;
        }
    `]
})
export class PositionComponent implements OnInit{
    @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
    
    positions: Array<Position>;
    position: Position;
    isDesc: boolean;
    column: string;
    direction: number;

    start: Event;
    pressed: boolean;
    startX;
    startWidth;
    p: number = 1;
    pageSize: number = 10;

    
    constructor(private renderer: Renderer, private positionService: PositionService){
        this.positions = new Array<Position>();
        this.loadPositions();
    }
    ngOnInit(){ }
    loadTemplate(position: Position) {
        return this.readOnlyTemplate;
    }
    sort(property: string){
        this.isDesc = !this.isDesc; //change the direction    
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    }
    public onMouseDown(event)
    {
        this.start = event.target;
        this.pressed = true;
        this.startX = event.x;
        this.startWidth = $(this.start).parent().width();
        this.initResizableColumns();
    }
    private loadPositions(){
        this.positionService.getPositions()
            .subscribe(data => {
                if(data.success){
                    this.positions = data.result;
                }
            }, error =>{console.log(error)});
    }
    public initResizableColumns() {
        this.renderer.listenGlobal('body', 'mousemove', (event) => {
           if(this.pressed) {
              let width = this.startWidth + (event.x - this.startX);
              $(this.start).parent().css({'min-width': width, 'max-   width': width});
              let index = $(this.start).parent().index() + 1;
              $('.glowTableBody tr td:nth-child(' + index + ')').css({'min-width': width, 'max-width': width});
              $('.glowTableBody thead tr td:nth-child(' + index + ')  input').css({'min-width': width-10, 'max-width': width-10});
           }
        });
        this.renderer.listenGlobal('body', 'mouseup', (event) => {
        if(this.pressed) {
            this.pressed = false;
        }
      });
   }
}