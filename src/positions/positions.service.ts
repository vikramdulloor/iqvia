import { Position } from './../positions/positions.model';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {Http,Response, Headers} from '@angular/http';
import { SharedService } from './../shared.service';

@Injectable()
export class PositionService{
    constructor(private http: Http, private sharedService: SharedService )
    {
        
    }
    getPositions(){
        const token = localStorage.getItem('token') ? localStorage.getItem('token') : '';
        const headers = new Headers({'Content-Type': 'application/json', 'Authorization': token});
        return this.http.get(this.sharedService.getUrl()+'me/positions', {headers: headers})
        .map((response: Response) => response.json())
        .catch((error: Response) =>  Observable.throw(error.json()));
    }
}