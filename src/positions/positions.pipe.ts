import {Pipe , PipeTransform} from '@angular/core';
@Pipe({  name: 'orderBy' })
export class OrderrByPipe implements PipeTransform {

    transform(records: Array<any>, args?: any): any {
        return records.sort(function(a, b){
            if(args.property == 'mDeviceName'){
                a[args.property] = (a[args.property]||"").toString().toLowerCase();
                b[args.property] = (b[args.property]||"").toString().toLowerCase();
            }
            if(a[args.property] < b[args.property]){
                return -1 * args.direction;
            }
            else if( a[args.property] > b[args.property]){
                return 1 * args.direction;
            }
            else{
                return 0;
            }
        });
    };
}