import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {Http,Response, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/Rx';

@Injectable()
export class SharedService{
    private url: string;
    userLoggedIn: boolean = true;
    constructor(private http: Http, private router: Router){
        if(!this.isLoggedIn()){
            this.userLoggedIn = false;
            this.router.navigate(['/auth', 'signin']);            
        }
        // this.url ='http://159.203.160.40/';
        this.url ='http://localhost:3000/';
    }
    public isLoggedIn(){
        return localStorage.getItem('token') !== null;
    }
    public getUrl(){
        return this.url;
    }
    public loggedIn(){
        this.userLoggedIn = true;
    }
    public logout(){
        this.userLoggedIn = false;
        localStorage.clear();
    }
    
}