import { OrderrByPipe } from './positions/positions.pipe';
import { PositionComponent } from './positions/positions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header.component';
import { SharedService } from './shared.service';
import { routing } from './app.routing';
import { DebounceKeyUpDirective } from './debounceKeyUp.directive';
import { SignupComponent } from './auth/signup.component';
import { AuthenticationComponent } from './auth/authentication.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { SigninComponent } from './auth/signin.component';
import { LogoutComponent } from './auth/logout.component';
import { AgmCoreModule } from '@agm/core';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AuthenticationComponent,
    SignupComponent,
    SigninComponent,
    LogoutComponent,
    DashboardComponent,
    PositionComponent,
    DebounceKeyUpDirective,
    OrderrByPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({apiKey: "AIzaSyDmxDNwLgfKG3qI0DWiuU99XLEPTcQAWso", libraries: ["places"]}),
    routing,
    NgxPaginationModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
