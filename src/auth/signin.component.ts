import { Router } from '@angular/router';
import { SharedService } from './../shared.service';
import { Component, OnInit } from '@angular/core';
import{ FormGroup, FormControl, Validators} from "@angular/forms";
import { User } from './user.model';
import { AuthService } from './auth.service';
@Component({
    selector: 'iqvia-signin',
    templateUrl: './signin.component.html',
    providers: [AuthService]
})
export class SigninComponent implements OnInit{
    myForm: FormGroup;
    data: any;
    constructor(private authService: AuthService, private sharedService: SharedService, private router: Router ){
        this.data = {success: false, message:""};
    }
    ngOnInit(){
        this.myForm = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
            password: new FormControl(null, Validators.required)
        });
    }
    onSubmit() {
        const user = new User(this.myForm.value.email, this.myForm.value.password);
        this.authService.signin(user)
            .subscribe( data => {
                if(data.success)
                {
                    localStorage.setItem('token', data.token);
                    this.sharedService.loggedIn();
                    this.router.navigateByUrl('/');
                }
                
            }, error => {this.data = error;});
        this.myForm.reset();
    }
}