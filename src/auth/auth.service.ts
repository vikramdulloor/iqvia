import { SharedService } from './../shared.service';
import { User } from './user.model';
import {Injectable} from '@angular/core';
import {Http,Response, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import 'rxjs/Rx';
@Injectable()
export class AuthService{
    
    constructor(private http: Http, private sharedService: SharedService )
    {
        
    }
    signup(user: User){
        const body = JSON.stringify(user);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(this.sharedService.getUrl()+'signup', body, {headers: headers})
        .map((response: Response) => response.json())
        .catch((error: Response) =>  Observable.throw(error.json()));
    }
    signin(user: User){
        const body = JSON.stringify(user);
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(this.sharedService.getUrl()+'login', body, {headers: headers})
        .map((response: Response) => response.json())
        .catch((error: Response) =>  Observable.throw(error.json()));
    }
}