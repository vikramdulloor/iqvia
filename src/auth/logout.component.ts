import { SharedService } from './../shared.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'iqvia-logout',
    template: `
        <div class="col-md-8 col-md-offset-2">
            <button class="btn btn-danger" (click) = "onLogout()">Logout</button>
        </div>
        `
})
export class LogoutComponent{
    constructor(private sharedService:SharedService, private router: Router){}
    onLogout(){
        this.sharedService.logout();
        this.router.navigate(['/auth', 'signin']);
    }
}