import { Component, OnInit } from '@angular/core';
import{ FormGroup, FormControl, Validators} from "@angular/forms";

import { User } from './user.model';
import { AuthService } from './auth.service';
@Component({
    selector: 'iqvia-signup',
    templateUrl: './signup.component.html',
    providers: [AuthService]
})
export class SignupComponent implements OnInit{
    myForm: FormGroup;
    messageFlag:boolean;
    message: string;
    constructor(private authService: AuthService){}
    ngOnInit() {
        this.myForm = new FormGroup({
            email: new FormControl("", [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$")]),
            password: new FormControl("", Validators.required),
            fullName: new FormControl('', Validators.required),
        });
    }
    onSubmit() {
        const user = new User(
            this.myForm.value.email, 
            this.myForm.value.password, 
            this.myForm.value.fullName, 
            );
            this.authService.signup(user)
                .subscribe(data => {
                    if(data.success){
                        this.messageFlag = data.success;
                        this.message = data.message;
                        this.myForm.reset();
                    }
                    setTimeout(function() {
                        this.messageFlag = false;
                        this.message = "";
                    }.bind(this), 3000);
                }, error => console.log(error));
        this.myForm.reset();
    }

}

