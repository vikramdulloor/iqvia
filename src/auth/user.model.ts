export class User {
    constructor(
        public email: string, 
        public password: string, 
        public fullName?: string,
        public _id?: string, 
        ){}
}