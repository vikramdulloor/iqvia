import { Component } from '@angular/core';
@Component({
    selector: 'iqvia-authentication',
    template: `
    <header class="row spacing">
        <nav class="col-md-8 col-md-offset-2">
            <ul class="nav nav-tabs">
            <li routerLinkActive='active'><a [routerLink]="['signup']">Register User</a></li>
            <li routerLinkActive='active'><a [routerLink]="['signin']">Signin</a></li>
                <!--
                <li routerLinkActive='active' *ngIf="isLoggedIn()"><a [routerLink]="['user']">B/O Users</a></li>                
                <li routerLinkActive='active' *ngIf="isAdmin()"><a [routerLink]="['signup']">Register User</a></li>
                <li routerLinkActive='active' *ngIf="!isLoggedIn()"><a [routerLink]="['signin']">Signin</a></li>
                <li routerLinkActive='active' *ngIf="isLoggedIn()"><a [routerLink]="['logout']">Logout</a></li>
                -->
            </ul>
        </nav>
    </header>
    <div class= "row spacing">
        <router-outlet></router-outlet>
    </div>
    `
})
export class AuthenticationComponent{

}