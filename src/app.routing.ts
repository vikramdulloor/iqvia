import { PositionComponent } from './positions/positions.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LogoutComponent } from './auth/logout.component';
import { AUTH_ROUTES } from './auth/auth.route';
import { AuthenticationComponent } from './auth/authentication.component';
import{ Routes, RouterModule } from  "@angular/router";

const APP_ROUTES: Routes = [
    {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    {path: 'auth', component: AuthenticationComponent, children: AUTH_ROUTES},
    {path: 'logout', component: LogoutComponent},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'positions', component: PositionComponent}
];
export const routing = RouterModule.forRoot(APP_ROUTES);