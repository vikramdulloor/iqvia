import { Directive, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';

@Directive({
  selector: '[appDebounceKeyUp]'
})
export class DebounceKeyUpDirective implements OnInit, OnDestroy {
  @Input() debounceTime = 500;
  @Output() debounceKeyup = new EventEmitter();
  private searchUpdated = new Subject();
  private subscription: Subscription;

  constructor() { }

  ngOnInit() {
    this.subscription = this.searchUpdated.debounceTime(this.debounceTime).subscribe(e => this.debounceKeyup.emit(e));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  @HostListener('keyup', ['$event'])
  keyUpEvent(event) {
    event.preventDefault();
    event.stopPropagation();
    this.searchUpdated.next(event);
  }
}