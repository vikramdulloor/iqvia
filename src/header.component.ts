import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { SharedService } from './shared.service';
@Component({
    selector: 'iqvia-header',
    template: `
        <header class="row">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li routerLinkActive="active" *ngIf="sharedService.userLoggedIn">
                            <a  [routerLink]="['/dashboard']">Dahsboard</a>
                        </li>
                        <li routerLinkActive="active" *ngIf="sharedService.userLoggedIn">
                            <a  [routerLink]="['/positions']">Positions</a>
                        </li>
                    </ul>
                    <ul class="nav pull-right">
                        <li routerLinkActive="active" *ngIf="!sharedService.userLoggedIn"><a  [routerLink]="['/auth']">Authentication</a></li>
                        <li routerLinkActive='active' *ngIf="sharedService.userLoggedIn">
                            <a [routerLink]="['logout']" (click) = "onLogout()">Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        `,
    styles:[`
        header{
            margin-top: 5px;
            padding: 5px;
            margin-bottom: 15px;
        }
        .noclick       {
            pointer-events: none;
          }
    `]
})
export class HeaderComponent{
    
    constructor(public sharedService:SharedService, private router: Router){
    }
    public onLogout(){
        this.sharedService.logout();
        this.router.navigate(['/auth', 'signin']);
    }
}

