import { DashboardService } from './dashboard.service';
import { Component, ElementRef, NgZone, OnInit, ViewChild  } from '@angular/core';
import{ FormGroup, FormControl, Validators} from "@angular/forms";
import { } from 'googlemaps';
import { MapsAPILoader } from '@agm/core';
import { Position } from '../positions/positions.model';
@Component({
    selector: 'iqvia-dashboard',
    templateUrl: './dashboard.component.html',
    providers: [DashboardService],
    styles:[`
    agm-map {
        height: 300px;
      }
    `]
})
export class DashboardComponent implements OnInit {
    
    @ViewChild("search") public searchElementRef: ElementRef;
    public latitude: number;
    public longitude: number;
    public searchControl: FormControl;
    public zoom: number;
    place: any;
    showFormFields: boolean = false;
    constructor(private mapsAPILoader: MapsAPILoader, private ngZone: NgZone, private dashboardService: DashboardService){}
    myForm: FormGroup;

    ngOnInit(){
        //set google maps defaults
        this.myForm = new FormGroup({
            searchControl : new FormControl('', Validators.required),
            fullName: new FormControl('', Validators.required),
            designation: new FormControl('', Validators.required),
            age: new FormControl('', Validators.required),
            salary: new FormControl('', Validators.required),
            startDate: new FormControl('', Validators.required),
            latitude: new FormControl(''),
            longitude: new FormControl('')
        });
        this.zoom = 4;
        this.latitude = 39.8282;
        this.longitude = -98.5795;

        //create search FormControl
        this.searchControl = new FormControl();

        //set current position
        this.setCurrentPosition();

        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["(cities)"]
            });
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                //get the place result
                let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    this.place = place;
                //verify result
                console.log(this.place);
                this.myForm.value.searchControl = this.place.vicinity;
                if (place.geometry === undefined || place.geometry === null) {
                    this.showFormFields = false;
                    return;
                }
                this.showFormFields = true;
                //set latitude, longitude and zoom
                this.latitude = place.geometry.location.lat();
                this.longitude = place.geometry.location.lng();
                this.zoom = 12;
                });
            });
        });
    }
    private setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
            this.latitude = position.coords.latitude;
            this.longitude = position.coords.longitude;
            this.zoom = 12;
            });
        }
    }
    onSubmit(){
        console.log(this.latitude, this.longitude);
        const ps = new Position(
            this.myForm.value.fullName,
            this.myForm.value.designation,
            this.place.vicinity,
            this.myForm.value.age,
            this.myForm.value.startDate,
            this.myForm.value.salary,
            this.latitude,
            this.longitude
            );
        console.log(ps);
        this.dashboardService.addPosition(ps)
            .subscribe(data => {
            if(data.success){
                // this.myForm.reset();
            }
            // setTimeout(function() {
                
            // }.bind(this), 3000);
        }, error => console.log(error));
        this.myForm.reset();
    }
}