process.env.NODE_ENV = 'test';
const mongoose = require("mongoose");
const User = require('../models/position');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let token = '';
describe('PositionRoutes', () => {
    beforeEach((done) => { //Before each test we empty the database
        let user = {email: 'vikramdulloor@gmail.com', password: 'iqvia_test'};
        chai.request(server)
                .post('/login')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('token');
                    this.token = res.body.token;
                    done();
                });    
    });
    describe('/POST Position', () => {
        it('it should POST New Position', (done) => {
            let position = 
            {
                fullName:'Vikram Dulloor',
                designation: 'Accountant',
                searchControl: 'Tokyo',
                age:50,
                startDate:"2018-01-12",
                salary:50000,
                latitude:"12.534",
                longitude:"3.6564"
            };
            chai.request(server)
                .post('/me/positions')
                .set('content-type', 'application/json')
                .set('Authorization', this.token)
                .send(position)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
            });
        });
    });
    describe('/GET Positions', () => {
        it('it should GET all Positions', (done) => {
            chai.request(server)
                .get('/me/positions')
                .set('content-type', 'application/json')
                .set('Authorization', this.token)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
            });
        });
    });
    
});