process.env.NODE_ENV = 'test';
const mongoose = require("mongoose");
const User = require('../models/user');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

describe('AppRoutes', () => {
    beforeEach((done) => { //Before each test we empty the database
        User.deleteAll( (err, result) => { 
           done();         
        });     
    });
    
    describe('/POST Signup', () => {
        it('it should POST New User', (done) => {
            let user = {email: 'vikramdulloor@gmail.com', password: 'iqvia_test', fullName: "Vikram Dulloor"};
            chai.request(server)
                .post('/signup')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.result.should.have.property('fullName');
                    res.body.result.fullName.should.be.eql(user.fullName);
                    res.body.result.should.have.property('password');
                    res.body.result.should.have.property('email');
                    res.body.result.email.should.be.eql(user.email);
                    done();
                });
        });
    });
    describe('/POST Login', () => {
        it('it should POST credentails and get response', (done) => {
            let user = {email: 'vikramdulloor@gmail.com', password: 'iqvia_test', fullName: "Vikram Dulloor"};
            User.addUser(user, (err, user) => {
                let login = {email: user.email, password: 'iqvia_test'};
                chai.request(server)
                .post('/login')
                .send(login)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('token');
                    res.body.result.should.have.property('fullName');
                    res.body.result.fullName.should.be.eql(user.fullName);
                    res.body.result.should.have.property('password');
                    res.body.result.should.have.property('email');
                    res.body.result.email.should.be.eql(user.email);
                    done();
                });
            });
        });
    });
});
