const path = require('path');
const webpack = require('webpack');
let webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.config.js');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
module.exports = webpackMerge.smart(commonConfig, {
    entry: {
        main: './src/main.aot.ts'
    },

    output: {
        path: path.resolve(__dirname + '/public/js/app'),
        filename: 'bundle.js',
        publicPath: '/js/app/',
        chunkFilename: '[id].[hash].chunk.js'
    },

    module: {
        rules: [
            {
                test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
                loader: '@ngtools/webpack'
            },
            {
                test: /\.ts$/,
                loaders: [
                    'awesome-typescript-loader',
                    'angular2-template-loader',
                ]
            },
            {
                test: /\.(ts|js)$/,
                use: [
                    'angular-router-loader?aot=true'
                ]
            }
        ]
    },

    plugins: [
        new UglifyJSPlugin({
            sourceMap: false
        }),
        
    ]
});