const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;
// const SchemaTypes = Schema.Types;
const SchemaColumns = {
    fullName: {type: String, required: true},
    designation: {type: String, required: true},
    searchControl: {type: String, required: true},
    age: {type: Number, required: true},
    startDate: {type: Date, required: true},
    salary: {type: Number, required: true},
    latitude: {type: String, required: true},
    longitude: {type: String, required: true},
};

const schema = new Schema(SchemaColumns);
        schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('positon', schema);