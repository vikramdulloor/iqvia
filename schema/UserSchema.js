const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;
// const SchemaTypes = Schema.Types;
const SchemaColumns = {
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    fullName: {type: String, required: true}
};

const schema = new Schema(SchemaColumns);
        schema.plugin(mongooseUniqueValidator);

module.exports = mongoose.model('users', schema);