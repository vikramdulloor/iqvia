const express     = require('express');
const path        = require('path');
const bodyParser  = require('body-parser');
const passport    = require('passport');
const mongoose    = require('mongoose');
const JOuth       = require('./bin/jOuth');
//================================================//

const Route         = './routes/';
const AppRoute      = require(Route+'app');
const UserRoute     = require(Route+"user");
const PositionRoute = require(Route+"position");

//================================================//
passport.use('jwt', JOuth); // To Be Filled
mongoose.connect('mongodb://localhost:27017/iqvia'); // To be Filled
let app = express();
    
    app.use(bodyParser.json({limit: '50mb', strict: false})); // Set up body-parser to access form body
    app.use(bodyParser.urlencoded({limit: '50mb',extended: false})); // Set up body-parser to access urlencoded data
     
    app.use(express.static(path.join(__dirname, 'public'))); // Expose public folder to be able to access js and stylesheets
    
    app.use(function (req, res, next) { // Middleware to process the request
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
        next();
    });
    
    
    
    
    app.route("/signup").post(AppRoute.signup);
    app.route('/login').post(AppRoute.login);
    
    app.route('/me/positions', passport.authenticate('jwt', { session: false }))
        .get(PositionRoute.getAllPositions)
        .post(PositionRoute.addPosition);
    app.route('/me', passport.authenticate('jwt', { session: false }), UserRoute.me)
    app.use("/", function(req, res, next) {
        res.sendFile(path.join(__dirname + '/public/index.html')); 
    });
    app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0");
module.exports = app;