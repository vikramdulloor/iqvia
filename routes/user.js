// let mongoose = require("mongoose");
// let User     = require("../models/user");
module.exports = {
    me: (req, res) => {
        res.status(201).json({success: true, message: "User Authenticated", result: req.user});
    }
};