let mongoose = require("mongoose");
let User     = require("../models/user");

module.exports = {
    signup: (req, res) => {
        User.validateEmail(req.body.email, (err, validEmail) => {
          if(err) return res.status(400).json({success:false, message: "There is some error in processing", error: err});
          else if(!validEmail) return res.status(400).json({success: false, message: "Email Already Used. Kindly Use Another"});
          else{
              User.addUser(req.body, (err, user) => {
                  if(err) return res.status(400).json({success:false, message: "There is some error in processing", error: err});
                  else if(!user) return res.status(400).json({success: false, message: "There is Some problem in signup Process. Please come back after sometime"});
                  else res.status(200).json({success: true, message: "User Signup Complete", result: user});
              });
          }
        }); 
    },
    
    login: (req, res)=> {
        User.getUserByEmail(req.body.email, (err, user) => {
            if(err) return res.status(400).json({success:false, message: "There is some error in processing", error: err});
            else if(!user) return res.status(400).json({success: false, message: "Invalid Email-Id"});
            else{
                User.comparePassword(req.body.password, user.password, (err, isMatch) => {
                    if(err) return res.status(400).json({success:false, message: "There is some error in processing", error: err});
                    else if(!isMatch) return res.status(400).json({success: false, message: "Invalid Email-Id"});
                    else {
                        const token = require("jsonwebtoken").sign({_id: user._id, email:user.email, password:user.password}, "c1FJhlEVCl8hud031aHc", {expiresIn: "60m"});
                        res.status(200).json({success: true, message: "User Signup Complete", result: user, token:"Bearer "+token});
                    }
                });
            }
        });
    }
}
