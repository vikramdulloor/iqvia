const Position = require('../models/position')
module.exports = {
    getAllPositions: (req, res) => {
        Position.getAllPositions( (err, positions) => {
            if(err) return res.status(400).json({success:false, message: "There is some error in processing", error: err});
            else{
                res.status(200).json({success:true, message: "Position List", result: positions});
            }
        });
    },

    addPosition: (req, res) => {
        Position.addPosition(req.body, (err, position) => {
            if(err) return res.status(400).json({success:false, message: "There is some error in processing", error: err});
            else{
                res.status(200).json({success:true, message: "Position Added", result: position});
            }
        });
    }
}