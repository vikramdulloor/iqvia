const mongoose = require('mongoose');
const User     = require("../schema/UserSchema");
const bcrypt   = require('bcryptjs');
module.exports = {
    getUserById: (id, cb) => {
        id = (!"object" === Object(id)) ? mongoose.Types.ObjectId(id) : id;
        User.findById(id, cb);
    },
    validateEmail: (email, cb) => {
        User.findOne({email: email}, (err, user) => {
            if(err) return cb(err, null);
            else if(!user) return cb(null, true)
            else cb(null, false);
        });
    },
    comparePassword: (myPassword, hash, cb) => {
        bcrypt.compare(myPassword, hash, (err, isMatch) =>{
            if(err) throw err;
            cb(null, isMatch);
        });
    },
    addUser: (columns, cb) => {
        let user = new User(columns);
        user.save((err, newUser) => {
            if(err) return cb(err, null);
            else{
                bcrypt.genSalt(10, (err, salt) => {
                    if(err) cb(err, null);
                    else{
                        bcrypt.hash(newUser.password, salt, function(err, hash){
                        if(err) console.log(err);
                        else{
                            newUser.password =  hash;
                            newUser.save(cb);
                        }
                    });
                  }
                });
            }
        });
    },
    
    getUserByEmail: (email, cb) => {
        User.findOne({email: email}, cb);
    },
    
    deleteAll: (cb) => {
        User.remove({}, cb);
    }
    
}