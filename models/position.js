const mongoose = require('mongoose');
const Position = require("../schema/PositionSchema");

module.exports = {
    addPosition: (columns, cb) => {
        let position = new Position(columns);
            position.save(cb);
    },
    getAllPositions: (cb) => {
        Position.find({}, cb);
    }
}
